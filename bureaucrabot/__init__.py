#!/usr/bin/python3

import json
import logging
import os
import requests
import sys

from centpkg.utils import determine_rhel_state, format_current_state_message

import git as gitpython
from . import utils as bureaucrabot_utils

# Phase Identifiers
# Phase 230 is "Planning / Development / Testing" (AKA DevTestDoc)
# Phase 450 is "Stabilization"
phase_devtestdoc = 230
phase_stabilization = 450


# PROJECT_OVERRIDE is for components that do not match between CentOS Stream
# and RHEL dist-git, most notably the components that contained '+' characters
# in their names. We *MUST* be sure that distrobaker is configured to redirect
# the component syncs properly before adding an entry to this list

PROJECT_OVERRIDE = {
    "dvdplusrw-tools": "dvd+rw-tools",
    "centos-release": "redhat-release",
    "compat-sap-cplusplus": "compat-sap-c++",
    "compat-sap-cplusplus-10": "compat-sap-c++-10",
    "compat-sap-cplusplus-11": "compat-sap-c++-11",
    "compat-sap-cplusplus-12": "compat-sap-c++-12",
    "compat-sap-cplusplus-13": "compat-sap-c++-13",
    "libsigcplusplus20": "libsigc++20",
    "memtest86plus": "memtest86+",
    "perl-Text-TabsplusWrap": "perl-Text-Tabs+Wrap",
}


class BureaucraBotException(Exception):
    pass


class BureaucrabotNoTicketException(BureaucraBotException):
    pass


def check_tickets(query_url, git_repo, namespace, project, oldrev, newrev, branch):
    logger = logging.getLogger(__name__)
    commits_data = []
    fixed = []

    # Extract the list of commits
    repo_commits = bureaucrabot_utils.get_commits(git_repo, oldrev, newrev)

    # Check if there is a ticket referenced in the Gitlab Merge Request
    mr_description = os.environ.get("CI_MERGE_REQUEST_DESCRIPTION", "").split("\n")
    logger.debug (f"Merge request description (may be truncated): {mr_description}")

    mr_resolved = bureaucrabot_utils.bugzillaIDs("Resolves?:", mr_description)
    mr_related = bureaucrabot_utils.bugzillaIDs("Related?:", mr_description)
    mr_reverted = bureaucrabot_utils.bugzillaIDs("Reverts?:", mr_description)
    fixed.extend(bureaucrabot_utils.bugzillaIDs("Fixes?:", mr_description))
    mr_tickets = mr_resolved + mr_related + mr_reverted

    if mr_tickets:
        # Get the list of of files that are modified in this set of commits
        # This is passed to the gitbz API so it will auto-ignore them if only
        # ignored files are included.
        all_changed_files = list()
        for repo_commit in repo_commits:
            changed_files = bureaucrabot_utils.get_commit_files(repo_commit)
            all_changed_files.extend(changed_files)

        # Create a fake "commit" to pass to the gitbz API.
        # The hexsha isn't actually validated, so it's convenient to use a
        # human-readable string for logging purposes.
        mr_commit = {
            "hexsha": "Merge Request Description",
            "files": all_changed_files,
            "resolved": mr_resolved,
            "related": mr_related,
            "reverted": mr_reverted,
        }
        commits_data.append(mr_commit)

    else:
        # Check the commit messages and bodies for tickets

        for repo_commit in repo_commits:
            changed_files = bureaucrabot_utils.get_commit_files(repo_commit)
            speclines = bureaucrabot_utils.specAdditions(project, repo_commit)
            message = bureaucrabot_utils.get_commit_message(repo_commit)

            logger.debug("Ticket data for %s: %s", repo_commit.hexsha, message + speclines)

            resolved = bureaucrabot_utils.bugzillaIDs("Resolves?:", message + speclines)
            related = bureaucrabot_utils.bugzillaIDs("Related?:", message + speclines)
            reverted = bureaucrabot_utils.bugzillaIDs("Reverts?:", message + speclines)
            fixed.extend(bureaucrabot_utils.bugzillaIDs("Fixes?:", message + speclines))
            commit_tickets = resolved + related + reverted

            # Gitlab merge request pipelines include an extra merge commit that
            # should be skipped. We can safely ignore any commit that changes
            # nothing at all. However, if an empty commit references a ticket,
            # we should still validate it.
            if not (changed_files + commit_tickets):
                logger.debug("Skipping empty commit")
                continue

            single_commit = {
                "hexsha": repo_commit.hexsha,
                "files": changed_files,
                "resolved": resolved,
                "related": related,
                "reverted": reverted,
            }
            commits_data.append(single_commit)

    request_data = {
        "package": project,
        "namespace": namespace,
        "ref": "refs/heads/{}".format(branch),
        "commits": commits_data,
    }

    logger.debug("Sending data to gitbz API to verify hooks result ...")
    logger.debug("Data sent: %s", json.dumps(request_data, sort_keys=True, indent=2))

    res = requests.post(query_url, json=request_data, timeout=905)

    logger.debug("Response from gitbz API: %s", res.text)
    try:
        res.raise_for_status()
    except requests.exceptions.RequestException as e:
        logger.warning("Failed ticket check for request: %s", request_data)
        logger.exception(e)
        raise BureaucraBotException("Invalid response from gitbz webservice API")

    payload = json.loads(res.text)

    logger.info(payload["logs"])
    if payload["result"] != "ok":
        # The gitbz API sends back the literal string
        # '${z-stream MISSING_BRANCH}' instead of replacing it with the prior
        # release branch, so we'll do a find-and-replace of it here, since we
        # know what it should be.
        error_message = payload["error"].replace(
            "${z-stream MISSING_BRANCH}", branch
        )
        logger.warning(error_message)

    # Check the payload for the string
    # "No issue IDs referenced in log message or changelog"
    if (
        not mr_tickets
        and "error" in payload
        and "No issue IDs referenced in log message or changelog" in payload["error"]
    ):
        raise BureaucrabotNoTicketException(
            "Merge requests must reference at least one ticket - either in the "
            "merge request description or in each individual commit. See "
            "https://one.redhat.com/rhel-development-guide/#con_rhel-devtestdoc-in-jira-new_rhel-devtestdoc-in-jira "
            "for more details."
        )

    if len(fixed) > 0:
        # These are bugs that were identified by the "Fixes:" prefix rather
        # than the "Resolves:" prefix.
        payload["logs"] += "\n"
        payload[
            "logs"
        ] += "*** WARNING: Detected one or more uses of 'Fixes:' instead of 'Resolves:'\n"
        payload["logs"] += "  Unverified:\n"
        for bug in fixed:
            payload["logs"] += "    {}\n".format(bug)

    return payload


def read_config(config_file):
    with open(config_file, "r") as f:
        c = json.load(f)
    return c


def log_setup():
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    # Log all messages into the debug log
    # Try to log to CI_PROJECT_DIR if available, otherwise to the current
    # directory.
    debug_handler = logging.FileHandler(f"{os.environ.get('CI_PROJECT_DIR', '.')}/debug.log")
    debug_handler.setLevel(logging.DEBUG)
    logger.addHandler(debug_handler)

    # Also log everything INFO and higher to the console
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)
    logger.addHandler(console_handler)

    return logger


def log_environment(logger):
    # The debug log should contain the environment variables we need to
    # exactly reproduce any issues, without including any secrets.
    logger.debug(f"Environment variables:")
    logger.debug(f"CI_COMMIT_SHA: {os.environ['CI_COMMIT_SHA']}")
    logger.debug(f"CI_MERGE_REQUEST_DESCRIPTION: {os.environ['CI_MERGE_REQUEST_DESCRIPTION']}")
    logger.debug(f"CI_MERGE_REQUEST_DIFF_BASE_SHA: {os.environ.get('CI_MERGE_REQUEST_DIFF_BASE_SHA', '')}")
    logger.debug(f"CI_MERGE_REQUEST_TARGET_BRANCH_NAME: {os.environ['CI_MERGE_REQUEST_TARGET_BRANCH_NAME']}")
    logger.debug(f"CI_PROJECT_DIR: {os.environ['CI_PROJECT_DIR']}")
    logger.debug(f"CI_PROJECT_NAME: {os.environ['CI_PROJECT_NAME']}")
    logger.debug(f"CI_PROJECT_NAMESPACE: {os.environ['CI_PROJECT_NAMESPACE']}")


def main():
    logger = log_setup()
    try:
        cfg = read_config("/etc/bureaucrabot.json")
    except FileNotFoundError as e:
        # Fall back to trying the local directory if we're testing in dev
        try:
            cfg = read_config("./bureaucrabot.json")
        except FileNotFoundError as e2:
            # Reraise the parent error so in production the error is clear
            raise e

    logger.debug("Working Directory: {}".format(os.getcwd()))
    log_environment(logger)

    namespace = os.environ["CI_PROJECT_NAMESPACE"].rsplit("/")[-1]
    centos_git_repo = gitpython.Repo(path=os.environ["CI_PROJECT_DIR"])

    project = os.environ["CI_PROJECT_NAME"]
    if project in PROJECT_OVERRIDE:
        project = PROJECT_OVERRIDE[project]

    rhel_state = determine_rhel_state(
        rhel_dist_git=cfg["rhel_dist_git"],
        namespace=namespace,
        repo_name=project,
        cs_branch=os.environ["CI_MERGE_REQUEST_TARGET_BRANCH_NAME"],
        pp_api_url=cfg["api_url"],
        distrobaker_config=cfg["distrobaker_config"],
    )

    logger.info(f"{format_current_state_message(rhel_state)}")

    # If this is an unsynced package, just approve it.
    if not rhel_state.synced:
        logger.info("Auto-approving CentOS Stream-only package")
        return

    # In the rare case where a merge request includes no file changes,
    # Gitlab does not set the $CI_MERGE_REQUEST_DIFF_BASE_SHA value.
    # In this situation, we'll treat the oldrev as the immediate
    # ancestor of the newrev.
    newrev = os.environ["CI_COMMIT_SHA"]
    oldrev = os.environ.get("CI_MERGE_REQUEST_DIFF_BASE_SHA", f"{newrev}^")

    try:
        result = check_tickets(
            query_url=cfg["gitbz_query_url"],
            git_repo=centos_git_repo,
            namespace=namespace,
            project=project,
            oldrev=oldrev,
            newrev=newrev,
            branch=rhel_state.rule_branch,
        )
    except BureaucrabotNoTicketException as e:
        # Log the exception message and return 1 (denied)
        logger.error(e)
        sys.exit(1)

    if result["result"] != "ok":
        if rhel_state.enforcing:
            logger.debug("Enforcing mode: deny this merge request")
            sys.exit(1)
        else:
            # We specifically treat exit(77) as a nonfatal failure
            logger.debug("Non-enforcing: permit this with warnings")
            sys.exit(77)


if __name__ == "__main__":
    main()
